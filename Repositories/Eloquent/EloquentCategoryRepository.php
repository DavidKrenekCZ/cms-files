<?php

namespace Modules\Files\Repositories\Eloquent;

use Modules\Files\Repositories\CategoryRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentCategoryRepository extends EloquentBaseRepository implements CategoryRepository
{
}
