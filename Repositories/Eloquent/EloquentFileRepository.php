<?php

namespace Modules\Files\Repositories\Eloquent;

use Modules\Files\Repositories\FileRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentFileRepository extends EloquentBaseRepository implements FileRepository
{
}
