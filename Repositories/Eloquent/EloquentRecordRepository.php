<?php

namespace Modules\Files\Repositories\Eloquent;

use Modules\Files\Repositories\RecordRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentRecordRepository extends EloquentBaseRepository implements RecordRepository
{
}
