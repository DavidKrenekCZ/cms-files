<?php

namespace Modules\Files\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface FileRepository extends BaseRepository
{
}
