<?php

namespace Modules\Files\Repositories\Cache;

use Modules\Files\Repositories\FileRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheFileDecorator extends BaseCacheDecorator implements FileRepository
{
    public function __construct(FileRepository $file)
    {
        parent::__construct();
        $this->entityName = 'files.files';
        $this->repository = $file;
    }
}
