<?php

namespace Modules\Files\Repositories\Cache;

use Modules\Files\Repositories\RecordRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheRecordDecorator extends BaseCacheDecorator implements RecordRepository
{
    public function __construct(RecordRepository $record)
    {
        parent::__construct();
        $this->entityName = 'files.records';
        $this->repository = $record;
    }
}
