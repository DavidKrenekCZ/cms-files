<?php

namespace Modules\Files\Repositories\Cache;

use Modules\Files\Repositories\CategoryRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheCategoryDecorator extends BaseCacheDecorator implements CategoryRepository
{
    public function __construct(CategoryRepository $category)
    {
        parent::__construct();
        $this->entityName = 'files.categories';
        $this->repository = $category;
    }
}
