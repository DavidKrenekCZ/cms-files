<?php

namespace Modules\Files\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileTranslation extends Model
{
	use SoftDeletes;
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'files__file_translations';
}
