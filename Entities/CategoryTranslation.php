<?php

namespace Modules\Files\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryTranslation extends Model
{
	use SoftDeletes;
    public $timestamps = false;
    protected $fillable = [
    	"name",
    	"slug"
    ];
    protected $table = 'files__category_translations';
}
