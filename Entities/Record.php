<?php

namespace Modules\Files\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Record extends Model {
    use SoftDeletes;
    use Translatable;

    public function category() {
        return $this->belongsTo("\Modules\Files\Entities\Category");
    }

    public function files() {
        return $this->hasMany("\Modules\Files\Entities\File");
    }

    protected $table = 'files__records';
    public $translatedAttributes = [
    	"name",
    	"description"
    ];
    protected $fillable = [
    	"active_to",
    	"active_from",
    	"category_id"
    ];

    public function getActiveToAttribute($value) {
        if ($value)
            return Carbon::parse($value);
        return false;
    }

    public function getActiveFromAttribute($value) {
        if ($value)
            return Carbon::parse($value);
        return false;
    }
}
