<?php

namespace Modules\Files\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model {
    use SoftDeletes;
    use Translatable;

    protected $table = 'files__categories';
    public $translatedAttributes = [
    	"name",
    	"slug"
    ];
    protected $fillable = [
    	"parent_id"
    ];
    protected $appends = ["parent", "children", "depth"];

    public function getParentAttribute() {
    	if ($this->parent_id == null)
    		return false;
    	return self::find($this->parent_id);
    }

    public function getChildrenAttribute() {
    	return self::where("parent_id", $this->id)->get();
    }

    public function getDepthAttribute() {
    	$x = $this->parent;
    	$depth = 1;
    	while ($x != false) {
    		$x = $x->parent;
    		$depth++;
    	}
    	return $depth;
    }

    public function path($separator=" / ") {
        $x = $this->parent;
        $result = $this->name;
        while ($x != false) {
            $result = $x->name.$separator.$result;
            $x = $x->parent;
        }
        return $result;
    }

    /*
    public function getAllChildren() {
    	$children = [];
    	foreach ($this->children as $child) {
    		$children[] = $child;
    		foreach ($child->getAllChildren() as $ch)
    			$children[] = $ch;
    	}

		$result = [];
		array_walk_recursive($children, function($v, $k) use (&$result){ $result[] = $v; });
    	return $result;
    }
    */
    
    public static function allMain() {
        return self::where("parent_id", null)->orderBy("position")->get();
    }

    public static function structuredAll() {
        /*
        $structured = [];
    	foreach (self::all() as $item) {
    		if (!$item->parent) {
    			$structured[] = $item;
    			foreach ($item->getAllChildren() as $value)
    				$structured[] = $value;
    		}
    	}
    	return $structured;*/
        $structured = [];
        foreach (self::allMain() as $main) {
            $structured[] = $main;
            foreach (self::where("parent_id", $main->id)->orderBy("position")->get() as $value)
                $structured[] = $value;
        }
        return $structured;
    }
}
