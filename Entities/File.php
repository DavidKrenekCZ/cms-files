<?php

namespace Modules\Files\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{
	use SoftDeletes;
    use Translatable;

    public function record() {
    	return $this->belongsTo("\Modules\Files\Entities\Record");
    }

    protected $table = 'files__files';
    public $translatedAttributes = [];
    protected $fillable = [];

    protected $appends = ["downloadUrl", "path", "deletedPath"];

    public function getDownloadUrlAttribute() {
    	return route("frontend.dk-files.download", [
    		"id" => $this->id,
    		"pass" => str_random(15).$this->pass.str_random(23),
    	]);
    }

    public function getPathAttribute() {
    	return public_path("/modules/files_module/".$this->filename);
    }

    public function getDeletedPathAttribute() {
    	return public_path("/modules/files_module/deleted/".$this->filename);
    }
}
