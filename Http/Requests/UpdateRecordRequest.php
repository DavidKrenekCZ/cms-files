<?php

namespace Modules\Files\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateRecordRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'active_to' => 'required_without:no_active_to',
            'active_from' => 'required'
        ];
    }

    public function translationRules()
    {
        return [
            //"name" => "required"
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
