<?php

namespace Modules\Files\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Files\Entities\File;
use Modules\Files\Entities\Record;
use Modules\Files\Http\Requests\CreateFileRequest;
use Modules\Files\Http\Requests\UpdateFileRequest;
use Modules\Files\Repositories\FileRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class FileController extends AdminBaseController {
    /**
     * @var FileRepository
     */
    private $file;

    public function __construct(FileRepository $file) {
        parent::__construct();

        $this->file = $file;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateFileRequest $request
     * @return Response
     */
    public function store(Record $record, Request $request) {
        if (!file_exists(public_path("/modules/files_module"))) {
            mkdir(public_path("/modules/files_module"));
            mkdir(public_path("/modules/files_module/deleted"));
        }

        if ($request->files->has("fls"))
            foreach ($request->files->get("fls") as $file) {
                $name = $file->getClientOriginalName();
                $explode = explode('.', $name);
                $extension = end($explode);
                do {
                    $filename = str_random(32).".".$extension;
                } while(file_exists(public_path("/modules/files_module/$filename")) or file_exists(public_path("/modules/files_module/deleted/$filename")));

                $file->move(public_path("/modules/files_module"), $filename);

                $f = new File();
                $f->name = $name;
                $f->filename = $filename;
                $f->record_id = $record->id;
                $f->pass = str_random(32);
                $f->save();
            }

        return redirect()->route('admin.files.record.edit', [
            "record" => $record
        ])
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('files::files.title.files')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  File $file
     * @return Response
     */
    public function destroy(File $file) {
        if (file_exists($file->path))
            rename($file->path, $file->deletedPath);

        $this->file->destroy($file);

        return redirect()->route('admin.files.record.edit', [
            "record" => $file->record
        ])
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('files::files.title.files')]));
    }

    public function download($id, $pass) {
        $file = File::findOrFail($id);
        if ($file->pass != substr($pass, 15, 32))
            abort(404);
        ob_end_clean();
        return response()->download($file->path, $file->name);
    }
}
