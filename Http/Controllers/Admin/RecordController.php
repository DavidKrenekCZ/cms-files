<?php

namespace Modules\Files\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Files\Entities\Record;
use Modules\Files\Http\Requests\CreateRecordRequest;
use Modules\Files\Http\Requests\UpdateRecordRequest;
use Modules\Files\Repositories\RecordRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class RecordController extends AdminBaseController
{
    /**
     * @var RecordRepository
     */
    private $record;

    public function __construct(RecordRepository $record)
    {
        parent::__construct();

        $this->record = $record;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $records = $this->record->all();

        return view('files::admin.records.index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('files::admin.records.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRecordRequest $request
     * @return Response
     */
    public function store(CreateRecordRequest $request) {
        $data = $request->all();

        $data["active_from"] = date("Y-m-d H:i", strtotime($data["active_from"]));

        if (isset($data["no_active_to"]) && $data["no_active_to"])
            $data["active_to"] = null;
        else
            $data["active_to"] = date("Y-m-d H:i", strtotime($data["active_to"]));

        $this->record->create($data);

        return redirect()->route('admin.files.record.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('files::records.title.records')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Record $record
     * @return Response
     */
    public function edit(Record $record)
    {
        return view('files::admin.records.edit', compact('record'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Record $record
     * @param  UpdateRecordRequest $request
     * @return Response
     */
    public function update(Record $record, UpdateRecordRequest $request)
    {
        $data = $request->all();

        $data["active_from"] = date("Y-m-d H:i", strtotime($data["active_from"]));

        if (isset($data["no_active_to"]) && $data["no_active_to"])
            $data["active_to"] = null;
        else
            $data["active_to"] = date("Y-m-d H:i", strtotime($data["active_to"]));

        $this->record->update($record, $data);

        return redirect()->route('admin.files.record.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('files::records.title.records')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Record $record
     * @return Response
     */
    public function destroy(Record $record)
    {
        $this->record->destroy($record);

        return redirect()->route('admin.files.record.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('files::records.title.records')]));
    }
}
