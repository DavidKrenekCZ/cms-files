<?php

namespace Modules\Files\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Files\Entities\Category;
use Modules\Files\Http\Requests\CreateCategoryRequest;
use Modules\Files\Http\Requests\UpdateCategoryRequest;
use Modules\Files\Repositories\CategoryRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class CategoryController extends AdminBaseController
{
    /**
     * @var CategoryRepository
     */
    private $category;

    public function __construct(CategoryRepository $category)
    {
        parent::__construct();

        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        $categories = Category::structuredAll();

        return view('files::admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('files::admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCategoryRequest $request
     * @return Response
     */
    public function store(CreateCategoryRequest $request) {
        $inputs = $request->all();
        if ((int)$inputs["parent_id"] == 0)
            $inputs["parent_id"] = null;
        $this->category->create($inputs);

        return redirect()->route('admin.files.category.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('files::categories.title.categories')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Category $category
     * @return Response
     */
    public function edit(Category $category)
    {
        return view('files::admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Category $category
     * @param  UpdateCategoryRequest $request
     * @return Response
     */
    public function update(Category $category, UpdateCategoryRequest $request) {
        $inputs = $request->all();
        if ((int)$inputs["parent_id"] == 0)
            $inputs["parent_id"] = null;
        $this->category->update($category, $inputs);

        return redirect()->route('admin.files.category.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('files::categories.title.categories')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Category $category
     * @return Response
     */
    public function destroy(Category $category)
    {
        $this->category->destroy($category);

        return redirect()->route('admin.files.category.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('files::categories.title.categories')]));
    }

    public function changePosition(Request $request) {
        foreach ($request->positions as $id => $position) {
            $cat = Category::find($id);
            $cat->position = $position;
            $cat->save();
        }
        return $request->positions;
    }
}
