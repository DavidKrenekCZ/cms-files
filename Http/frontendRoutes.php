<?php

$router->get('/dk-files/download/{id}/{pass}', [
    'as' => 'frontend.dk-files.download',
    'uses' => 'Admin\FileController@download',
]);