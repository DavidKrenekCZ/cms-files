<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/files'], function (Router $router) {
    $router->post("categories/change-position", [
        "as" => "admin.files.category.change-position",
        "uses" => "CategoryController@changePosition",
        "middleware" => 'can:files.categories.index'
    ]);

    $router->bind('category', function ($id) {
        return app('Modules\Files\Repositories\CategoryRepository')->find($id);
    });
    $router->get('categories', [
        'as' => 'admin.files.category.index',
        'uses' => 'CategoryController@index',
        'middleware' => 'can:files.categories.index'
    ]);
    $router->get('categories/create', [
        'as' => 'admin.files.category.create',
        'uses' => 'CategoryController@create',
        'middleware' => 'can:files.categories.create'
    ]);
    $router->post('categories', [
        'as' => 'admin.files.category.store',
        'uses' => 'CategoryController@store',
        'middleware' => 'can:files.categories.create'
    ]);
    $router->get('categories/{category}/edit', [
        'as' => 'admin.files.category.edit',
        'uses' => 'CategoryController@edit',
        'middleware' => 'can:files.categories.edit'
    ]);
    $router->put('categories/{category}', [
        'as' => 'admin.files.category.update',
        'uses' => 'CategoryController@update',
        'middleware' => 'can:files.categories.edit'
    ]);
    $router->delete('categories/{category}', [
        'as' => 'admin.files.category.destroy',
        'uses' => 'CategoryController@destroy',
        'middleware' => 'can:files.categories.destroy'
    ]);



    $router->bind('record', function ($id) {
        return app('Modules\Files\Repositories\RecordRepository')->find($id);
    });
    $router->get('records', [
        'as' => 'admin.files.record.index',
        'uses' => 'RecordController@index',
        'middleware' => 'can:files.records.index'
    ]);
    $router->get('records/create', [
        'as' => 'admin.files.record.create',
        'uses' => 'RecordController@create',
        'middleware' => 'can:files.records.create'
    ]);
    $router->post('records', [
        'as' => 'admin.files.record.store',
        'uses' => 'RecordController@store',
        'middleware' => 'can:files.records.create'
    ]);
    $router->get('records/{record}/edit', [
        'as' => 'admin.files.record.edit',
        'uses' => 'RecordController@edit',
        'middleware' => 'can:files.records.edit'
    ]);
    $router->put('records/{record}', [
        'as' => 'admin.files.record.update',
        'uses' => 'RecordController@update',
        'middleware' => 'can:files.records.edit'
    ]);
    $router->delete('records/{record}', [
        'as' => 'admin.files.record.destroy',
        'uses' => 'RecordController@destroy',
        'middleware' => 'can:files.records.destroy'
    ]);



    $router->bind('file', function ($id) {
        return app('Modules\Files\Repositories\FileRepository')->find($id);
    });
    $router->post('files/{record}', [
        'as' => 'admin.files.file.store',
        'uses' => 'FileController@store',
        'middleware' => 'can:files.records.edit'
    ]);
    $router->delete('files/{file}', [
        'as' => 'admin.files.file.destroy',
        'uses' => 'FileController@destroy',
        'middleware' => 'can:files.records.destroy'
    ]);
// append



});
