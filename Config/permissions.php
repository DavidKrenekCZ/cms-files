<?php

return [
    'files.categories' => [
        'index' => 'files::categories.list resource',
        'create' => 'files::categories.create resource',
        'edit' => 'files::categories.edit resource',
        'destroy' => 'files::categories.destroy resource',
    ],
    'files.records' => [
        'index' => 'files::records.list resource',
        'create' => 'files::records.create resource',
        'edit' => 'files::records.edit resource',
        'destroy' => 'files::records.destroy resource',
    ],
// append



];
