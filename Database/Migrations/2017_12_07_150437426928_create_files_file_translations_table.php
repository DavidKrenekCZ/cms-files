<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesFileTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files__file_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('file_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['file_id', 'locale']);
            $table->foreign('file_id')->references('id')->on('files__files')->onDelete('cascade');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('files__file_translations', function (Blueprint $table) {
            $table->dropForeign(['file_id']);
        });
        Schema::dropIfExists('files__file_translations');
    }
}
