<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesRecordTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files__record_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            
            $table->string("name");
            $table->string("description", 1000)->nullable();

            $table->integer('record_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['record_id', 'locale']);
            $table->foreign('record_id')->references('id')->on('files__records')->onDelete('cascade');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('files__record_translations', function (Blueprint $table) {
            $table->dropForeign(['record_id']);
        });
        Schema::dropIfExists('files__record_translations');
    }
}
