<?php

namespace Modules\Files\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterFilesSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('files::files.title.files'), function (Item $item) {
                $item->icon('fa fa-file-pdf-o');
                $item->weight(10);
                $item->authorize(
                     $this->auth->hasAccess('files.categories.index') ||
                     $this->auth->hasAccess('files.records.index')
                );
                $item->item(trans('files::categories.title.categories'), function (Item $item) {
                    $item->icon('fa fa-circle-o');
                    $item->weight(0);
                    $item->append('admin.files.category.create');
                    $item->route('admin.files.category.index');
                    $item->authorize(
                        $this->auth->hasAccess('files.categories.index')
                    );
                });
                $item->item(trans('files::records.title.records'), function (Item $item) {
                    $item->icon('fa fa-circle-o');
                    $item->weight(0);
                    $item->append('admin.files.record.create');
                    $item->route('admin.files.record.index');
                    $item->authorize(
                        $this->auth->hasAccess('files.records.index')
                    );
                });
            });
        });

        return $menu;
    }
}
