<?php

namespace Modules\Files\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Files\Events\Handlers\RegisterFilesSidebar;

class FilesServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterFilesSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('categories', array_dot(trans('files::categories')));
            $event->load('records', array_dot(trans('files::records')));
            $event->load('files', array_dot(trans('files::files')));
            // append translations



        });
    }

    public function boot()
    {
        $this->publishConfig('files', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Files\Repositories\CategoryRepository',
            function () {
                $repository = new \Modules\Files\Repositories\Eloquent\EloquentCategoryRepository(new \Modules\Files\Entities\Category());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Files\Repositories\Cache\CacheCategoryDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Files\Repositories\RecordRepository',
            function () {
                $repository = new \Modules\Files\Repositories\Eloquent\EloquentRecordRepository(new \Modules\Files\Entities\Record());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Files\Repositories\Cache\CacheRecordDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Files\Repositories\FileRepository',
            function () {
                $repository = new \Modules\Files\Repositories\Eloquent\EloquentFileRepository(new \Modules\Files\Entities\File());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Files\Repositories\Cache\CacheFileDecorator($repository);
            }
        );
// add bindings



    }
}
