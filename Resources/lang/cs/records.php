<?php

return [
    'list resource' => 'Procházet záznamy',
    'create resource' => 'Vytvářet záznam',
    'edit resource' => 'Upravovat záznamy',
    'destroy resource' => 'Mazat záznamy',
    'title' => [
        'records' => 'Záznamy',
        'create record' => "Vytvořit záznam",
        'edit record' => 'Upravit záznam',
    ],
    'button' => [
        'create record' => 'Vytvořit záznam',
        "upload"    => "Nahrát",
        "add file"  => "Přidat další soubor"
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
        "upload availability" => "Nahrávání souborů bude dostupné po uložení základních informací"
    ],
    'validation' => [
    ],
    "fields" => [
        "category_id" => "Kategorie",
        "name"  => "Název",
        "description" => "Popis",
        "active_from" => "Datum zveřejnění",
        "active_to"     => "Datum skrytí",
        "no_active_to"  => "Neskrývat",
        "no_of_files"   => "Počet souborů",
        "files"         => "Soubory"
    ]
];
