<?php

return [
    'list resource' => 'Procházet soubory',
    'create resource' => 'Vytvářet soubory',
    'edit resource' => 'Upravovat soubory',
    'destroy resource' => 'Mazat soubory',
    'title' => [
        'files' => 'Soubor',
    ],
    'messages' => [
        "error" => "Při nahrávání souboru se vyskytla chyba, opakujte akci"
    ],
];
