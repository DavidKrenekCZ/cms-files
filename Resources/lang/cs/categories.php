<?php

return [
    'list resource' => 'Procházet kaetgorie',
    'create resource' => 'Vytvářet kategorii',
    'edit resource' => 'Upravovat kategorie',
    'destroy resource' => 'Mazat kategorie',
    'title' => [
        'categories' => 'Kategorie',
        'create category' => 'Vytvořit kategorii',
        'edit category' => 'Upravit kategorii',
    ],
    'button' => [
        'create category' => 'Vytvořit kategorii',
    ],
    'table' => [
        "path" => "Cesta",
        "name" => "Název",
        "ajax error" => "Vyskytla se chyba, opakujte akci"
    ],
    'form' => [
        "no parent" => "(hlavní kategorie)"
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    "fields" => [
        "name" => "Název",
        "slug" => "Slug",
        "parent" => "Nadřazená kategorie"
    ],    
];
