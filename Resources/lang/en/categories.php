<?php

return [
    'list resource' => 'List categories',
    'create resource' => 'Create categories',
    'edit resource' => 'Edit categories',
    'destroy resource' => 'Destroy categories',
    'title' => [
        'categories' => 'Categories',
        'create category' => 'Create a category',
        'edit category' => 'Edit a category',
    ],
    'button' => [
        'create category' => 'Create a category',
    ],
    'table' => [
        "path" => "Path",
        "name" => "Name",
        "ajax error" => "An error has occured, please try again"
    ],
    'form' => [
        "no parent" => "(no parent)"
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    "fields" => [
        "name" => "Name",
        "slug" => "Slug",
        "parent" => "Parent category"
    ],    
];
