<?php

return [
    'list resource' => 'List records',
    'create resource' => 'Create a record',
    'edit resource' => 'Edit records',
    'destroy resource' => 'Destroy records',
    'title' => [
        'records' => 'Records',
        'create record' => 'Create a record',
        'edit record' => 'Edit a record',
    ],
    'button' => [
        'create record' => 'Create a record',
        "upload"    => "Upload",
        "add file"  => "Add another file"
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
        "upload availability" => "File uploading will be available after initial info is set"
    ],
    'validation' => [
    ],
    "fields" => [
        "category_id" => "Category",
        "name"  => "Name",
        "description" => "Description",
        "active_from" => "Active from",
        "active_to"     => "Active to",
        "no_active_to"  => "Forever",
        "no_of_files"   => "Number of files",
        "files"         => "Files"
    ]
];
