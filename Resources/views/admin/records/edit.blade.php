@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('files::records.title.edit record') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.files.record.index') }}">{{ trans('files::records.title.records') }}</a></li>
        <li class="active">{{ trans('files::records.title.edit record') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('files::admin.records.partials.edit-fields', ['lang' => $locale])
                        </div>
                    @endforeach

                    <hr>
                    {!! Form::open(['route' => ['admin.files.record.update', $record->id], 'method' => 'put']) !!}
                    <div class="box-body">
                        <div class="form-group{{ $errors->has("parent_id") ? ' has-error' : '' }}">
                            <label>{{ trans('files::records.fields.category_id') }}</label>
                            <select class="form-control" name="category_id">
                                @foreach(\Modules\Files\Entities\Category::structuredAll() as $cat)
                                <option value="{{ $cat->id }}" {{ old("category_id") == $cat->id ? 'selected' : ($record->category_id == $cat->id ? "selected" : "") }}>
                                    {{ str_repeat("- ", $cat->depth-1) }} {{ $cat->name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group{{ $errors->has("active_from") ? ' has-error' : '' }}">
                            <label>{{ trans("files::records.fields.active_from") }}</label>
                            <div class='input-group date datetimepicker'>
                                <input type='text' class="form-control" value="{{ old("active_from", $record->active_from->format("d. m. Y H:i")) }}" name="active_from"/>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has("active_to") ? ' has-error' : '' }}">
                            <label>{{ trans("files::records.fields.active_to") }}</label>
                            <div class='input-group date datetimepicker'>
                                <input type='text' class="form-control" value="{{ old("active_to", $record->active_to ? $record->active_to->format("d. m. Y H:i") : "") }}" name="active_to"/>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                                <span class="input-group-addon">
                                    <input type="checkbox" class="flat-blue" name="no_active_to" {{ old("no_active_to") ? "checked" : (!$record->active_to ? "checked" : "") }}>&nbsp;&nbsp;{{ trans("files::records.fields.no_active_to") }}
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.files.record.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ trans("files::records.fields.files") }}</h3>
                </div>
                <div class="box-body">
                    @foreach($record->files as $file)
                        <div class="a-file">
                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                            {{ $file->name }}
                            <a href="{{ $file->downloadUrl }}">
                                <i class="fa fa-download" aria-hidden="true"></i>
                            </a>
                            <a data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.files.file.destroy', [$file->id]) }}">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                        </div>
                    @endforeach
                    <hr style="margin-bottom: 0">
                </div>
                {!! Form::open(['route' => ['admin.files.file.store', $record->id], "files" => true]) !!}
                <div class="box-body">
                    <div class="file-inputs">
                        <input type="file" name="fls[]">
                    </div>
                    <a class="add-file-button">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        {{ trans("files::records.button.add file") }}
                    </a>
                    <div class="file-preloader">
                        <div class="el-loading-spinner spinner"><svg viewBox="25 25 50 50" class="circular"><circle cx="50" cy="50" r="20" fill="none" class="path"></circle></svg><!----></div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-flat upload-files">{{ trans("files::records.button.upload") }}</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push("css-stack")
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" integrity="sha256-yMjaV542P+q1RnH6XByCPDfUFhmOafWbeLPmqKh11zo=" crossorigin="anonymous" />
    <style type="text/css">
        .add-file-button {
            display: block;
            margin-top: 1rem;
            cursor: pointer;
        }

        .file-inputs input[type=file] {
            margin-bottom: 5px;
        }

        .file-preloader {
            display: none;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(255,255,255,.5)
        }

        .a-file {
            padding: 3px 0;
        }

        .a-file .fa {
            margin: 0 5px;
        }

        .a-file a {
            cursor: pointer;
        }
    </style>
@endpush

@push('js-stack')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/moment.min.js" integrity="sha256-/As5lS2upX/fOCO/h/5wzruGngVW3xPs3N8LN4FkA5Q=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/locale/cs.js" integrity="sha256-5e0RubO3OCaMrr1X80YLiI1pxD6oYnnkLMSBXPpVu9E=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js" integrity="sha256-5YmaxAwMjIpMrVlK84Y/+NjCpKnFYa8bWWBbUHSBGfU=" crossorigin="anonymous"></script>
    <script type="text/javascript">
        function checkActiveTo() {
            if ($("input[type=checkbox][name=no_active_to]:checked").length)
                $("input[type=checkbox][name=no_active_to]").closest(".input-group").find("input[type=text]").attr("disabled", "disabled");
            else
                $("input[type=checkbox][name=no_active_to]").closest(".input-group").find("input[type=text]").removeAttr("disabled");
        }

        $( document ).ready(function() {
            $('.datetimepicker').datetimepicker({
                locale: '{{ locale() }}',
                allowInputToggle: true
            });

            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "@php route('admin.files.record.index') @endphp" }
                ]
            });
        });

        $(window).load(function() {
            checkActiveTo();
            $("input[type=checkbox][name=no_active_to]").on('ifToggled', checkActiveTo);
        });


        $(document).ready(function() {
            $(".add-file-button").click(function() {
                $(".file-inputs").append("<input type='file' name='fls[]'>");
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush
