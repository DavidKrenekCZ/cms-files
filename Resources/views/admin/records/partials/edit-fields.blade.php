<div class="box-body">
    <div class='form-group{{ $errors->has("{$lang}.name") ? ' has-error' : '' }}'>
        {!! Form::i18nInput("name", trans('files::records.fields.name'), $errors, $lang, $record) !!}
    </div>
    <div class='form-group{{ $errors->has("{$lang}.description") ? ' has-error' : '' }}'>
        @editor('description', trans('files::records.fields.description'), old("{$lang}.description", $record->hasTranslation($lang) ? $record->description : ''), $lang)
    </div>
</div>
