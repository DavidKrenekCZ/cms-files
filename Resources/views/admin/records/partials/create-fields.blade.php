<div class="box-body">
    <div class='form-group{{ $errors->has("{$lang}.name") ? ' has-error' : '' }}'>
        {!! Form::i18nInput("name", trans('files::records.fields.name'), $errors, $lang) !!}
    </div>
    <div class='form-group{{ $errors->has("{$lang}.description") ? ' has-error' : '' }}'>
        @editor('description', trans('files::records.fields.description'), old("{$lang}.description"), $lang)
    </div>
</div>
