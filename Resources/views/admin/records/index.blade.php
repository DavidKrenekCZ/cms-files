@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('files::records.title.records') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('files::records.title.records') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.files.record.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('files::records.button.create record') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{ trans('files::records.fields.name') }}</th>
                                <th>{{ trans('files::records.fields.category_id') }}</th>
                                <th>{{ trans('files::records.fields.active_from') }}</th>
                                <th>{{ trans('files::records.fields.active_to') }}</th>
                                <th>{{ trans('files::records.fields.no_of_files') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($records)): ?>
                            <?php foreach ($records as $record): ?>
                            <tr>
                                <td>
                                    <a href="{{ route('admin.files.record.edit', [$record->id]) }}">
                                        {{ $record->name }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.files.record.edit', [$record->id]) }}">
                                        {{ $record->category->name }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.files.record.edit', [$record->id]) }}">
                                        {{ $record->active_from->format("j. n. Y H:i") }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.files.record.edit', [$record->id]) }}">
                                        {{ $record->active_to ? $record->active_to->format("j. n. Y H:i") : trans('files::records.fields.no_active_to') }}
                                    </a>
                                </td>
                                <td>
                                    {{ $record->files->count() }}
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.files.record.edit', [$record->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.files.record.destroy', [$record->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('files::records.title.create record') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.files.record.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
