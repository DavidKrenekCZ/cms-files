@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('files::categories.title.categories') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('files::categories.title.categories') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.files.category.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('files::categories.button.create category') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="position: relative">
                    <div class="table-responsive">
                        <table class="table-striped table table-bordered table-hover sortable">
                            <thead>
                            <tr data-parent-id="0">
                                <th>{{ trans('files::categories.table.name') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($categories)): ?>
                            <?php foreach ($categories as $category): ?>
                            <tr data-parent-id="{{ $category->parent_id or "0" }}" data-id="{{ $category->id }}">
                                <td>
                                    <a href="{{ route('admin.files.category.edit', [$category->id]) }}">
                                        {{ str_repeat("&nbsp;", ($category->depth-1)*10) }} {{ $category->name }}
                                    </a>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.files.category.edit', [$category->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.files.category.destroy', [$category->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                    <div class="cat-preloader">
                        <div class="el-loading-spinner spinner"><svg viewBox="25 25 50 50" class="circular"><circle cx="50" cy="50" r="20" fill="none" class="path"></circle></svg><!----></div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('files::categories.title.create category') }}</dd>
    </dl>
@stop

@push("css-stack")
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css">
    <style type="text/css">
        .ui-state-highlight {
            border: 1px solid #dad55e !important;
            background: #fffa90 !important;
            color: #777620 !important;
        }

        .ui-state-highlight-disabled {
            background: #b4b4b4 !important;
        }

        tr.ui-sortable-handle {
            cursor: move;
        }

        .cat-preloader {
            display: none;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(255,255,255,.5)
        }
    </style>
@endpush

@push('js-stack')
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $("table.table-striped.table-bordered.sortable tbody").sortable({
                placeholder: "ui-state-highlight",

                // Element dropped
                update: function(e, ui) {
                    // Dragging is not valid - remove
                    if ($(ui.placeholder).hasClass("ui-state-highlight-disabled"))
                        return false;

                    // Sort all subcategories after their parents
                    $(".sortable tr[data-parent-id=0]").each(function() {
                        var el = $(this);
                        var children = $("tr[data-parent-id='"+el.data("id")+"']");
                        for (var i = children.length - 1; i >= 0; i--)
                            $(children[i]).insertAfter(el);
                    });

                    var data = {};
                    $(".sortable tr[data-id]").each(function() {
                        var id = $(this).data("id");
                        var position = $(this).index()+1;
                        data[id] = position;
                    });

                    $(".cat-preloader").show();
                    $.ajax("{{ route("admin.files.category.change-position") }}", {
                        type: "post",
                        data: {
                            _token: "{{ csrf_token() }}",
                            positions: data
                        }
                    }).fail(function(e) {
                        console.log(e);
                        alert("{{ trans('files::categories.table.ajax error') }}");
                    }).always(function() {
                        $(".cat-preloader").hide();
                    });
                },

                // Element dragged, but not dropped
                change: function(e, ui) {
                    var el = $(ui.placeholder);                     // drop placeholder
                    var parent_id = $(ui.item).data("parent-id");   // ID of parent of dragged element
                    var id = $(ui.item).data("id");                 // ID of dragged element

                    // Next and previous rows
                    var nx = el.next("tr");
                    var pv = el.prev("tr");

                    // Subcategory is not next to sibling subcategories -> disable placeholder
                    if (
                        parent_id != 0 &&
                        nx.data("parent-id") != parent_id && pv.data("parent-id") != parent_id
                    )
                        el.addClass("ui-state-highlight-disabled");
                    else
                        el.removeClass("ui-state-highlight-disabled");
                },

                // Sorting starts
                start: function(e, ui) {
                    // If we're sorting main categories, hide subcategories
                    if ($(ui.item).data("parent-id") == 0)
                        $(".sortable tr[data-parent-id][data-parent-id!=0]").addClass("hide");
                },

                // Sorting stops
                stop: function(e, ui) {
                    // Show all subcategories
                    $(".sortable tr[data-parent-id][data-parent-id!=0]").removeClass("hide");
                }
            });

            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.files.category.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
