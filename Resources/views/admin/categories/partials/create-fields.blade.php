<div class="box-body">
    <div class='form-group{{ $errors->has("{$lang}.name") ? ' has-error' : '' }}'>
        {!! Form::i18nInput("name", trans('files::categories.fields.name'), $errors, $lang, null, ["data-slug" => "source"]) !!}
    </div>
    <div class='form-group{{ $errors->has("{$lang}.slug") ? ' has-error' : '' }}'>
        {!! Form::i18nInput("slug", trans('files::categories.fields.slug'), $errors, $lang, null, ["data-slug" => "target"]) !!}
    </div>
</div>
